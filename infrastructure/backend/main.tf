#+--------------------------------------------------------------------+
#|                             Variables                              |
#+--------------------------------------------------------------------+

variable "aws_access_key" {
    type        = string
    description = "Access key for AWS user"
}

variable "aws_secret_key" {
    type        = string
    description = "Secret key for AWS user"
}

variable "region" {
    default     = "eu-west-2"
    type        = string
    description = "AWS Region to use"
}

variable "bucket_name" {
    type = string
    description = "name for the pipeline S3 bucket"
}

#+--------------------------------------------------------------------+
#|                             Providers                              |
#+--------------------------------------------------------------------+

provider "aws" {
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key
    region     = var.region
}

#+--------------------------------------------------------------------+
#|                           Pipeline Bucket                          |
#+--------------------------------------------------------------------+

resource "aws_s3_bucket" "pipeline_bucket" {
  bucket = var.bucket_name
  acl = "private"
  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = "pipeline-bucket"
  }

}

#+--------------------------------------------------------------------+
#|                         Remote State Lock                          |
#+--------------------------------------------------------------------+

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "remote-state-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}